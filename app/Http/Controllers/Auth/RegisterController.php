<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Role;
use DB; 

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/usuario';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:2', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    
    protected function create(array $data)
    {
        $usuario =  User::create([
            'name' => $data['name'],
            'apellido_p' => $data['apellido_p'],
            'apellido_m' => $data['apellido_m'],
            'email' => $data['email'],
            'nocontrol' => $data['nocontrol'],
            'colonia' => $data['colonia'],
            'calle' => $data['calle'],
            'telefono' => $data['telefono'],
            'password' => Hash::make($data['password']),
        ]);
        
         $id_user=$usuario->id;
        
        
        $role_id = $data["role"];
        
        $role = new Role();
        $user = new User();
        
          $role_user = [
            ['role_id' => $role_id,
            'user_id' => $id_user]
        ];
        DB::table('role_user')->insert($role_user);
       
        return $usuario;
    }
    
      protected function agregar(array $data)
    {
        $usuario =  User::create([
            'name' => $data['name'],
            'apellido_p' => $data['apellido_p'],
            'apellido_m' => $data['apellido_m'],
            'email' => $data['email'],
            'nocontrol' => $data['nocontrol'],
            'colonia' => $data['colonia'],
            'calle' => $data['calle'],
            'telefono' => $data['telefono'],
            'password' => Hash::make($data['password']),
        ]);
        
         $id_user=$usuario->id;
        
        
        $role_id = $data["role"];
        
        $role = new Role();
        $user = new User();
        
          $role_user = [
            ['role_id' => $role_id,
            'user_id' => $id_user]
        ];
        DB::table('role_user')->insert($role_user);
       
        return $usuario;
    }
}
