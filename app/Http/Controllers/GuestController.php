<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        $libros = DB::table('libros')->orderBy('id', 'desc')->take(5)->get();
        $apartados = DB::table('apartados')->get();
        return view('cliente.index', compact('libros', 'apartados'));
    }
    
    public function catalogo()
    {
        $libros = DB::table('libros')->orderBy('id', 'desc')->take(5)->get();
        $apartados = DB::table('apartados')->get();
        
        return view('cliente.libros', compact('libros','apartados'));
    }
    public function autores()
    {
        return view('cliente.autores');
    }
    public function info()
    {
        return view('cliente.info');
        
    }public function carrito()
    {
        $apartados = DB::table('apartados')->get();
        $libros = DB::table('libros')->get();
        return view('cliente.carrito', compact('apartados', 'libros'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
