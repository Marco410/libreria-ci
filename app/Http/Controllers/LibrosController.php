<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\libro;
use App\Apartado;
use DB;

class LibrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {  
        $request->user()->authorizeRoles(['admin','manejador','prestador']);
        
        $libro= libro::All();
        $numero = libro::count();
        $total = libro::sum('ejemplares');

      


        return view('admin.book.index', compact('libro', 'numero', 'total'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      

        return view('admin.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $libro= libro::All();
        return view('admin.book.show', compact('libro'));
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        return view('admin.book.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver(Request $request)
    {   $id = $request->input('id');

        $libro = new Libro();
        $libro = libro::where('id', $id)->get();
        
     return view('admin.book.libro', compact("libro"));
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
         $id=$request->input('id');

        $librob = new libro();
        $librob = libro::find($id);
        $librob->delete(); 

      
       
        
     return redirect('/libro/show');

              
        
        return redirect('/libro/show');

    }
    public function registrar(Request $request){

        $libror = new libro();

        $libror->titulo=$request->input('titulo');
        $libror->autor=$request->input('autor');
        $libror->edicion=$request->input('edicion');
        $libror->fecha_publicacion=$request->input('fecha_publicacion');
        $libror->editorial=$request->input('editorial');
        $libror->contenido=$request->input('contenido');
        $libror->tipo_material=$request->input('tipo_material');
        $libror->isbn=$request->input('isbn');
        $libror->clasi_dewey=$request->input('clasi_dewey');
        $libror->ejemplares=$request->input('ejemplares');
        $libror->save();
        $libro= libro::All();
        return redirect('/libro/show');
       
        // return view('admin.book.show', compact('libro'));
    
    }
    public function buscar(Request $request){

        $option=$request->input('option');
        $busqueda =$request->input('search');
        $libro = new libro();
        if($option==1){
            $libro = libro::where('autor', $busqueda)->get();
            return view('admin.book.show', compact('libro'));
        }else if($option==2){
            $libro = libro::where('titulo', $busqueda)->get();
            return view('admin.book.show', compact('libro'));
         }else if($option==3){ 
             $libro = libro::where('clasi_dewey', $busqueda)->get();
            return view('admin.book.show', compact('libro'));}
            else{
                $libro = libro::where('tipo_material', $busqueda)->get();
                return view('admin.book.show', compact('libro'));

            }

     
    }
    public function editar(){
        $libror = new libro();

        $libror->titulo=$request->input('titulo');
        $libror->autor=$request->input('autor');
        $libror->edicion=$request->input('edicion');
        $libror->fecha_publicacion=$request->input('fecha_publicacion');
        $libror->editorial=$request->input('editorial');
        $libror->contenido=$request->input('contenido');
        $libror->tipo_material=$request->input('tipo_material');
        $libror->isbn=$request->input('isbn');
        $libror->coleccion=$request->input('coleccion');
        $libror->clasi_dewey=$request->input('clasi_dewey');
        $libror->clasi_local=$request->input('clasi_local');
        $libror->ejemplares=$request->input('ejemplares');
        $libror->notas=$request->input('notas');
        $libror->nota_biblio=$request->input('notas_biblio');
        $libror->save();
        return "hecho";
    }

    public function apartar(Request $request, $id_libro){
        $apartado = new Apartado();
        $apartado->id_libro = $id_libro;
        $apartado->id_user = 1;
        $apartado->save();
        return redirect('/');
    }
    
    public function eliminar_apartado(Request $request, $id_apartado)
    {
        $libro = Apartado::find($id_apartado);
        $libro->delete();
        return redirect('carrito');
    }
}
