<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pedido;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.pedidos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pedidos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.pedidos.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.pedidos.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function registrar(Request $request){

        $pedido = new pedido();
        $pedido->titulo=$request->input('titulo');
        $pedido->autor=$request->input('autor');
        $pedido->edicion=$request->input('edicion');
        $pedido->fecha_publicacion=$request->input('fecha_publicacion');
        $pedido->editorial=$request->input('editorial');
        $pedido->tipo_material=$request->input('tipo_material');
        $pedido->academia=$request->input('option');
        $pedido->ejemplares=$request->input('ejemplares');
        $pedido->save();
        $pedidos= pedido::All();
       
        return view('admin.pedidos.show', compact('pedidos'));
    
    }
}
