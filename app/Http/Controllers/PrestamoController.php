<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prestamo;
use App\libro;
use App\cliente;
use Carbon\Carbon;
use App\User;
class PrestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //$fecha = new DateTime();
        //echo $fecha->format('Y-m-d H:i:s');
        //$libros = libro::All();
        //$clientes = cliente::All();
        $prestamo = prestamo::orderBy('id', 'ASC')->where('estado', 'PRESTADO')->paginate(4);
        //->where('fecha_entrega', '$fecha')->paginate();
        //return view('admin.prestamo.index');// compact('prestamos', 'libros','clientes'));
        //$prestamo= prestamo::All();
        return view('admin.prestamo.index', compact('prestamo'));
    }

    public function verifica(){
        
        return view('admin.prestamo.verifica');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.prestamo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,['isbn'=> 'required', 'nombre_material'=> 'required', 
        'nombre_alumno'=>'required','no_control'=>'required','estado'=> 'required',
         'fecha_prestamo'=> 'required', 'fecha_entrega'=> 'required']);
        $prestamo  = prestamo::create($request->all());
        return redirect()->route('prestamo.create', $prestamo->id)
        ->with('info','Prestamo registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::orderBy('id', 'ASC')->where('name', '')->paginate(1);

        return view('admin.prestamo.show', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestamo = prestamo::find($id);
        return view('admin.prestamo.edit', compact('prestamo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $prestamo = prestamo::find($id);
        $prestamo->isbn = $request->get('isbn');
        $prestamo->nombre_material = $request->get('nombre_material');
        $prestamo->nombre_alumno = $request->get('nombre_alumno');
        $prestamo->no_control = $request->get('no_control');
        $prestamo->estado = $request->get('estado');
        $prestamo->fecha_prestamo = $request->get('fecha_prestamo');
        $prestamo->fecha_entrega = $request->get('fecha_entrega');
        $prestamo->save();
        return redirect()->route('prestamo.index', $prestamo->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buscarPrestamo(Request $request){

        $busqueda =$request->input('search');
        $prestamo = new prestamo();
            $prestamo = prestamo::where('no_control', $busqueda)->get();
            return view('admin.prestamo.index', compact('prestamo'));
     
    }

    public function buscarUsuario(Request $request){

        $busqueda =$request->input('search');
        $users = new user();
            $users = user::where('nocontrol', $busqueda)->get();
            return view('admin.prestamo.show', compact('users'));
     
    }
    public function entregar(Request $request,$id){
        $prestamo = prestamo::find($id);
        DB::table('prestamos')
            ->where('id', $id)
            ->update(['estado' => 'ENTREGADO']);
            
        return redirect()->route('prestamo.index', $prestamo->id);
     
    }
}
