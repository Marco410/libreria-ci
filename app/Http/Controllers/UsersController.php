<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Auth;
use DB;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admin');
        
         $users = User::paginate(4);
        return view('admin.usuarios.index', compact('users'));
    }
    
     public function perfil(Request $request)
    {
       $users = User::all();
        
        return view('admin.usuarios.perfil', compact('users','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('admin');
        return view('admin.usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $request->user()->authorizeRoles('admin');
       return view('admin.usuarios.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $request->user()->authorizeRoles('admin');
        
        $user = User::findOrFail($id);
        
        return view('admin.usuarios.edit', compact('user'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        
        $user->name = $request->get('name');
        $user->apellido_p = $request->get('apellido_p');
        $user->apellido_m = $request->get('apellido_m');
        $user->email = $request->get('email');
        $user->colonia = $request->get('colonia');
        $user->calle = $request->get('calle');
        $user->telefono = $request->get('telefono');
        $user->nocontrol = $request->get('nocontrol');
        $user->save();
        
        $role_id = $request->get("role");
        
       

        DB::table('role_user')->where('user_id', $id)->update(['role_id' => $role_id]);
        
        return redirect('usuario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table('role_user')->where('user_id', $id)->delete();
        
        $user = User::find($id);
        
        $user->delete();
        
        return redirect('usuario');
    }
}
