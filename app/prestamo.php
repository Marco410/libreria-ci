<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $fillable = [
        'isbn', 'nombre_material','nombre_alumno', 'no_control',
         'estado', 'fecha_prestamo', 'fecha_entrega',
    ];
    
}
