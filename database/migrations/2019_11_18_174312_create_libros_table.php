<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('autor');
            $table->string('edicion');
            $table->string('fecha_publicacion');
            $table->string('editorial')->nullable();
            $table->string('contenido')->nullable();
            $table->string('tipo_material');
            $table->string('isbn')->require();
            $table->string('coleccion')->nullable();
          
            $table->string('clasi_dewey');
            $table->string('clasi_local')->nullable();
            $table->string('ejemplares');
            $table->string('notas')->nullable();
            $table->string('nota_biblio')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
