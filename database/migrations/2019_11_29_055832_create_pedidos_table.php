<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            Schema::dropIfExists('pedidos');
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('autor');
            $table->string('edicion');
            $table->string('fecha_publicacion');
            $table->string('editorial');
            $table->string('tipo_material');
            $table->string('academia');
            $table->string('ejemplares');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
