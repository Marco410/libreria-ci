@extends('layouts.app')

@section('title')
Crear Libro
@endsection
    
@section('content')

  <h4 style="font-family: 'Poppins', sans-serif;" class="center"> Nuevo material</h4> 

    <div class="row" >

    <div class="col m12 ">


  <!--   <div class="card-panel teal lighten-2 center-align" style="margin-top:5%; margin-left: 5%;"><h4>Registrar Libro</h4> </div> -->
           <form action="{{ action('LibrosController@registrar') }}" method="post">
           
               
            <div class="card " style="margin-top:5%;">
            @csrf 
            <div  class="input-fiel col s12">
            <div class="input-fiel col s6">
            <select>
                                          <option value="" disabled selected>Tipo de material</option>
                                          <option value="1">Libro</option>
                                          <option value="2">CD</option>
                                          <option value="3">Tesis</option>
                                          <option value="3">Revista</option>
                                    </select>
                                  </div>
              </div>
         
                                   
                
            
              <div class="input-fiel col s4">                
                    <label for="titulo">Titulo</label>
                  <input type="text" id="titulo" name="titulo">

              </div>
              <div class="input-fiel col s4">
                    <label for="autor">Autor</label>
                    <input type="text" id="autor" name="autor">
                    
              </div>
              <div class="input-fiel col s4">
                    <label for="edicion">Edicion</label>
                    <input type="text" id="edicion" name="edicion">
                    
              </div>
              <div class="input-fiel col s6 datepicker">                
                    <label for="fecha">Fecha de publicación</label>
                  <input type="text" id="fecha" class="datepicker" name="fecha_publicacion">

              </div>
              <div class="input-fiel col s6">
                    <label for="editorial">Editorial</label>
                    <input type="text" id="editorial" name="editorial">
                    
              </div>
              <div class="input-fiel col s6">
                    <label for="contenido">Contenido</label>
                    <input type="text" id="contenido" name="contenido">
                    
              </div>
              <div class="input-fiel col s3">
                    <label for="tipo_material">Tipo de material</label>
                    <input type="text" id="tipo_material" name="tipo_material">
                    
              </div>
              <div class="input-fiel col s3">
                    <label for="isbn">ISBN</label>
                    <input type="text" id="isbn" name="isbn">
                    
              </div>

              <div class="input-fiel col s6">
                    <label for="clasi_dewey">Clasificación Dewey</label>
                    <input type="text" id="clasi_dewey" name="clasi_dewey">
                    
              </div>

              <div class="input-fiel col s6">
                    <label for="ejemplares">Numero de ejemplares</label>
                    <input type="text" id="ejemplares" name="ejemplares">
                    
              </div>
              

              
        
              <div class="center" >

              
              
              <button  onclick="M.toast({html: 'Registro Exitoso!'})" href="" class="btn waves-effect waves-light " style="border-radius:10px;" type="submit" name="" >Registrar</button>
                <button class="btn waves-effect waves-light" style="border-radius:10px;"type="reset" name="action">Cancelar</button>
              </div>
              
              
               
                        
              </div>
              </form>
        </div>
    </div>
   <script> M.AutoInit();
   M.toast({html: 'Registro Exitosos!'})</script>

@endsection