@extends('layouts.app')

@section('title')
Libros
@endsection
    
@section('content')
<div class="row" >
    
            <div class="col s12" >
                <h3 class="left blue-grey-text lighten-3-text" >Material</h3>
            </div>
            
            <div class="col s3" >
                
                  <div class="card-panel center cyan lighten-1" >
                    <h4 class="white-text">{{$numero}}</h4>
                    <h7 class="white-text">TOTAL DE LIBROS</h7>
                </div>
                
                </div>
            <div class="col s3" >
          <div class="card-panel center teal lighten-1" >
                    <h4 class="white-text">74</h4>
                    <h7 class="white-text">lIBROS PRESTADOS</h7>
                </div>
                </div>
            <div class="col s3" >
                <div class="card-panel center orange lighten-1" >
                    <h4 class="white-text">{{$total}}</h4>
                    <h7 class="white-text">LIBROS REGRESADOS</h7>
                </div>
                </div>
            <div class="col s3" >
                <div class="card-panel center deep-purple lighten-1" >
                    <h4 class="white-text">1</h4>
                    <h7 class="white-text">LIBROS PERDIDOS</h7>
                </div>
         </div>
            

            
         
               
          
          
        
        </div>
        

@endsection