@extends('layouts.app')

@section('title')
Editar Libro
@endsection
    
@section('content')


    <div class="row" >
    <div class="col s12 ">@foreach($libro as $libro)
               <div class="card grey lighten-2">  <p class="center"
                style="font-family: 'Sulphur Point', sans-serif;; font-size: 25px;">
                {{$libro->titulo}}</p>
                </div> 
    
    </div>


   <div class="col s12 grey lighten-4">
   
     <div class="col s2 "> </div>
     <div class="col s5 grey lighten-4">
            <p style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Titulo:</p>
          <p style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Autor:</p>
          <p style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Editorial:</p>
          <P style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Año de Publicación:</P>
          <P style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Tipo de material:</P>
          <P style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Código:</P>
          <P style="font-family: 'Sulphur Point', sans-serif; font-size: 20px;">Contenido:</P>  
            </div>
            
            <div class="col s5 grey lighten-4 " style="line-height: 38px ">
            <p style="font-family: 'Poiret One', cursive; 30px;">{{$libro->titulo}}</p>
            <p style="font-family: 'Poiret One', cursive; 30px;">{{$libro->autor}}</p>
            <p style="font-family: 'Poiret One', cursive; 20px;">{{$libro->editorial}}</p>
            <p style="font-family: 'Poiret One', cursive; 20px;">{{$libro->fecha_publicacion}}</p>
            <p style="font-family: 'Poiret One', cursive; 20px;">{{$libro->tipo_material}}</p>
            <p style="font-family: 'Poiret One', cursive; 20px;">{{$libro->isbn}}</p>
            <p style="font-family: 'Poiret One', cursive; 20px;">{{$libro->contenido}}</p> 
          
            </div>
            @endforeach
            <a href="http://localhost/BIBLIOTECA/libreria-ci/public/libro/show" class="center">Ver todos los libros</a>
   
   </div>


    
    </div>

@endsection