@extends('layouts.app')

@section('title')
Mostrar Libro
@endsection
    
@section('content')



    
<div class="col m12 "> 
<form action="{{ action('LibrosController@buscar') }}" method="post">
@csrf 
            <div class="row">
            <div class="col m4"> 
                              <div class="input-field col s12" name="option" >
                              @csrf
                                            <select name="option" value="">
                                              <option value="0" disabled selected>Buscar</option>
                                              <option value="1" name="option"  >Autor</option>
                                              <option value="2" >Titulo</option>
                                              <option value="3" >Clasificación</option>
                                              <option value="4" >Tipo</option>
                                            </select>
                    
                    </div>

           
                 </div>


            <div class="col m6">
             
                 <div class="" style="margin-top:8%;">
                  
                        <div class="nav-wrapper  blue lighten-5" style="background-color:; border-radius:8px;">
                              <form>
                                <div class="input-field">
                                  <input id="search" type="search" name="search" required>
                                  <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                  <i class="material-icons">close</i>
                                </div>
                              </form>
                            </div>
                          
                    </div>
                    
                    </div>
                    <div class="col m2" style="margin-top:3%;">
                    <button class="btn waves-effect waves-light"  style=" border-radius:15px;"type="submit" name="action">Buscar
                <i class="material-icons right">search</i>
              </button></div>
              </form> 
                 </div>
               
                 
           
           
           <div class="col m12"> <div class="card  " style="margin-top: 5%; " >
            <table>
                        <thead>
                          <tr>
                              <th>Libro</th>
                              <th>ISBN</th>
                              <th>Ejemplares</th>
                              <th>Acciones</th>
                          </tr>
                        </thead>
                        @foreach($libro as $libro) 
                        <tbody class="hoverable">
                          <tr>
                          <form action="{{ action('LibrosController@ver') }}" method="post"></form>
                            <td ><a href=""><span class="blue-text">{{$libro->titulo}}</span> </a>  <br>
                            </form>
                            <label for="">Autor: {{$libro->autor}}</label><br>
                        
                            </td >
                            <td>{{$libro->isbn}}</td>
                            <td>{{$libro->ejemplares}}</td>
                            
                            <td>
                            <form action="{{ action('LibrosController@destroy') }}"  method="post">
             
             @csrf
                         
                                         <input type="hidden" value="{{$libro->id}}" name="id"> 
                
                            <button class="btn waves-effect waves-light"  style=" border-radius:15px; "type="submit" name="action">Eliminar</button>
                           
                            </form>
                            <form action="{{ action('LibrosController@ver') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{$libro->id}}" name="id"> 
                            <button class="btn waves-effect waves-light"  style=" border-radius:15px; margin-top:5%;"type="submit" name="action">Ver</button>
                            </form>
                           
                       
                        
                                          
                            </td>
                          </tr>
                         
                         
                       
                         
                        </tbody>
                        @endforeach
                      </table>
                    
          </div>
        
          </div>
           
  

          
        </div> 




@endsection