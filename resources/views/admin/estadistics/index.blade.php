@extends('layouts.app')

@section('title')
Estadisticas
@endsection
    
@section('content')

<h3>Estadisticas</h3>
    
    <div class="row" >
        <form action={{ URL::asset('/estadisticas/') }} method="POST">
            @csrf
            <div class="col m6">
                <select name="value">
                    <option value="1">Libros más solicitados</option>
                    <option value="2">Estudiantes con más préstamos</option>
                </select>
            </div>
            <div class="col m6">
                <input type="submit" class="btn">
            </div>
        </form>
    </div>
    @if ($stats)
        @if ($value == 1)
            <table>
                <thead>
                    <tr>
                        <th>Libro</th>
                        <th>Préstamos</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stats as $stat)
                    <tr>
                        <th>
                            @foreach ($type as $libro)
                                @if ($libro->isbn == $stat->isbn_libro)
                                    {{$libro->titulo}}
                                @endif
                            @endforeach
                        </th>
                        <th>{{$stat->prestamos}}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @elseif ($value == 2)
            <table>
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Préstamos</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stats as $stat)
                    <tr>
                        <th>
                            @foreach ($type as $user)
                                @if ($user->id == $stat->id_usuario)
                                    {{$user->name}}
                                @endif
                            @endforeach
                        </th>
                        <th>{{$stat->prestamos}}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    @endif

@endsection