@extends('layouts.app')

@section('title')
Mostrar Pedido
@endsection
    
@section('content')

<br>
<h4 style="font-family: 'Poppins', sans-serif;" class="center">Lista de pedidos pedido</h4> 

<br>

<div class="row">
  <div class="col m12"> <div class="card  " style="margin-top: 5%; " >
         
        <table>
                 <thead>
                   <tr>
                       <th>Libro</th>
                       <th>Academia</th>
                       <th>Ejemplares</th>
                       <th>Edicion</th>
                       <th>Editorial</th>
                   </tr>
                 </thead>
                 <tbody>
                 @foreach($pedidos as $pedido)
                                    <tr>
                                        <td>{{ $pedido->titulo }}</td>
                                        <td>{{ $pedido->academia }}</td>
                                        <td>{{ $pedido->ejemplares }}</td>
                                        <td>{{ $pedido->edicion }}</td>
                                        <td>{{ $pedido->editorial }}</td>

                                        <td width="10px">
                                            <a href="#" class="btn btn-sm btn-default">
                                                Editar
                                            </a>
                                        </td>
                                        <td width="10px">
                                            <button class="btn btn-sm red">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                 
                 
                 </tbody>

        </table>

</div>

@endsection