@extends('layouts.app')

@section('title')
Crear Prestamo
@endsection
    
@section('content')
<br/> <br/>

<div class="container"   >
<div class="row justify-content-center" >
  <div class="col-md-6" >
    <div class="card" style="padding:45px">
    
  <form method="POST" action="{{ route('prestamo.store') }}">
     @csrf
    <div class="form-group">
        <label for="formGroupExampleInput2">Código del material</label>
        <input type="text" class="form-control" id="codigoMaterial"
        placeholder="1235" name="isbn">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput2">Título del material</label>
        <input type="text" class="form-control" id="titulo"
        placeholder="Titulo" name="nombre_material">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput2">Nombre del usuario</label>
        <input type="text" class="form-control" id="nombreUser"
        placeholder="Nombre Apellido" name="nombre_alumno">
    </div>
    <div class="form-group">
        <label for="formGroupExampleInput2">No. de Control</label>
        <input type="text" class="form-control" id="nocontrol"
        placeholder="12131415" name="no_control">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="formGroupExampleInput2"
         name="estado" value="PRESTADO">
    </div>
    <h7>Fecha de prestamo</h7>
    <?php $fcha = date("Y-m-d");?>
    <input type="date" class="form-control"  value="<?php echo $fcha;?>" name="fecha_prestamo">
    <h7>Fecha de entrega</h7>
    <?php $fcha = date("Y-m-d");?>
    <input type="date" class="form-control"  value="<?php echo $fcha;?>" name="fecha_entrega" id="fecha_entrega">

    <center>

        <button id= "btnImprimir" onclick="setDoc()" type="" class="btn btn-primary" style="margin: 10px">
                {{ __('Imprimir') }}
    </button>
    <button id= "btnRegistrar" type="submit" class="btn btn-primary" style="margin: 10px">
                {{ __('Registrar') }}
    </button>
    </center>
      </form>
    <input type="hidden" value="{{ Auth::user()->name  }}" id="nombrebiblio" name="nombreBiblio" />



      
  </div>
  </div>
</div>
</div>

<script>
document.getElementById('btnRegistrar').disabled = true;
</script>

@endsection