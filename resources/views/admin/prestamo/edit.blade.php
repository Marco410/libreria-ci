@extends('layouts.app')

@section('title')
Crear Prestamo
@endsection
    
@section('content')

<div class="container"   >
<div class="row justify-content-center" >
  <div class="col-md-6" >
    <div class="card" style="padding:45px">
  <form method="POST" action="{{  route('prestamo.update', $prestamo->id) }}">
  {{ method_field('put') }}
            {{ csrf_field() }}
            <div class="form-group">
        <label for="formGroupExampleInput2">Código del material</label>
        <input type="text" class="form-control" id="formGroupExampleInput2"
         name="isbn" value="{{$prestamo->isbn}}">
    </div>
     <div class="form-group">
      <label for="formGroupExampleInput2">Título del material</label>
      <input type="text" class="form-control" id="formGroupExampleInput2"
      placeholder="Titulo" name="nombre_material" value="{{$prestamo->nombre_material}}">
      </div>
      <div class="form-group">
          <label for="formGroupExampleInput2">Nombre del usuario</label>
          <input type="text" class="form-control" id="formGroupExampleInput2"
          placeholder="Nombre Apellido" name="nombre_alumno" value="{{$prestamo->nombre_alumno}}">
      </div>
      <div class="form-group">
          <label for="formGroupExampleInput2">No. de Control</label>
          <input type="text" class="form-control" id="formGroupExampleInput2"
          placeholder="12131415" name="no_control" value="{{$prestamo->no_control}}">
      </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Estado del Prestamo</label>
    <select class="form-control" id="exampleFormControlSelect1" name="estado" required>
      <option value="PRESTADO" >PRESTADO</option>
      <option  vaelu= "ENTREGADO">ENTREGADO</option>
    </select>
  </div>
      <h7>Fecha de prestamo</h7>
      <?php $fcha = date("Y-m-d");?>
      <input type="date" class="form-control"  value="{{$prestamo->fecha_prestamo}}" name="fecha_prestamo">
      <h7>Fecha de entrega</h7>
      <?php $fcha = date("Y-m-d");?>
      <input type="date" class="form-control"  value="{{$prestamo->fecha_entrega}}" name="fecha_entrega">

    <center>
    <button type="submit" class="btn btn-primary" style="margin: 10px">
                {{ __('Registrar') }}
    </button>
    </center>
      </form>
  </div>
  </div>
</div>
</div>

@endsection