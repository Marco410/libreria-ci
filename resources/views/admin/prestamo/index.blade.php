@extends('layouts.app')

@section('title')
Prestamos
@endsection
    
@section('content')

<br/><br/><br/>
<div class="col m12 "> 
<form action="{{ action('PrestamoController@buscarPrestamo') }}" method="post">
@csrf
            <div class="row">
            <div class="col m6">
             
                 <div class="" style="margin-top:8%;">
                        <div class="nav-wrapper  blue lighten-5" style="background-color:; border-radius:8px;">
                              <form>
                                <div class="input-field">
                                  <input id="search" type="search" name="search" required>
                                  <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                  <i class="material-icons">close</i>
                                </div>
                              </form>
                            </div>     
                    </div>
                    
                    </div>
                    <div class="col m2" style="margin-top:3%;">
                    <button class="btn waves-effect waves-light"  style=" border-radius:15px;"type="submit" name="action">Buscar
                <i class="material-icons right">search</i>
              </button></div>
              </form> 

    <table class="card" style="margin-top: 5%" >
                    <thead style="background: #FFFFFF">
                        <tr>
                            <th>Codigo</th>
                            <th>Titulo</th>
                            <th>Usuario</th>
                            <th>No. Control</th>
                            <th>Estado</th>
                            <th>Fecha de préstamo</th>
                            <th>Fecha de entrega</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                
                    <tbody style="background: #FFFFFF">
                    @foreach($prestamo as $prestamoL)
                        <tr>
                            <td>{{$prestamoL->isbn}}</td>
                            <td>{{$prestamoL->nombre_material}}</td>
                            <td>{{$prestamoL->nombre_alumno}}</td>
                            <td>{{$prestamoL->no_control}}</td>
                            <td>{{$prestamoL->estado}}</td>
                            <td>{{$prestamoL->fecha_prestamo}}</td>
                            <td>{{$prestamoL->fecha_entrega}}</td>
                            <td>
                             <a class="waves-effect waves-light btn-small" href="{{ route('prestamo.edit', $prestamoL->id )}}"><i class="material-icons">edit</i></a>                             
                            </td>
                        </tr>                    
                    @endforeach
                    </tbody>
        </table>

         


@endsection