@extends('layouts.app')

@section('title')
Mostrar Prestamo
@endsection
    
@section('content')

<h3>Buscar usuario</h3>

<br/><br/><br/>
<div class="col m12 "> 
<form action="{{ action('PrestamoController@buscarUsuario') }}" method="post">
@csrf
            <div class="row">
            <div class="col m6">
             
                 <div class="" style="margin-top:8%;">
                 <h5>Inserte número de control</h5>
                        <div class="nav-wrapper  blue lighten-5" style="background-color:; border-radius:8px;">
                              <form>
                                <div class="input-field">
                                  <input id="search" type="search" name="search" required>
                                  <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                  <i class="material-icons">close</i>
                                </div>
                              </form>
                            </div>     
                    </div>
                    
                    </div>
                    <div class="col m2" style="margin-top:3%;">
                    <button class="btn waves-effect waves-light"  style=" border-radius:15px;"type="submit" name="action">Buscar
                <i class="material-icons right">search</i>
              </button>
              </div>
              

    <table class="card" style="margin-top: 5%" >
                    <thead style="background: #FFFFFF">
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>No. Control</th>
                        </tr>
                    </thead>
                
                    <tbody style="background: #FFFFFF">
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->nocontrol}}</td>
                        </tr>                    
                    @endforeach
                    </tbody>
        </table>
        </form> 
@endsection
