@extends('layouts.app')

@section('title')
Crear Prestamo
@endsection
    
@section('content')

<h3>Vista para los Verificar un Prestamo</h3>

<div class="container">
    <h3 class="center-align">VERIFICAR</h3>
    <div class="row">
    <!-- Comienzo de la columna que muestra información del usuario-->
        <div class="col s6">
    <!-- Inicia formulario que nos ayudará a mostrar los datos del usuario "jalados" de l BD
    así mismo mostrará si el alumno tiene un adeudo-->
        <h6>Buscar usuario</h6>
            <div class="row">
                <div class="col s7">
                <nav>
                    <form>
                    <div class="nav-wrapper blue"style=" border-radius:8px;" >
                        <div class="input-field" style="border-radius:15px;">
                        <input id="search" type="search" required>
                        <label class="label-icon" for="search"><i class="material-icons">search</i>Inserte N° de control</label>
                        <i class="material-icons">close</i>
                        </div>
                    </form>
                    </div>
                </nav> 
                </div>
                <div class="col s4">
                <a href="" class="btn btn-sm btn" style="background: #3AAF88">Buscar</a>
                </div>
            </div>
            <br/><br/>
            <div class="row">
                <div class="input-field">
                    <i class="material-icons prefix">perm_identity</i>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" required>
                </div>
                <div class="input-field">
                    <i class="material-icons prefix">person_pin</i>
                    <label for="apellido">Apellidos</label>
                    <input type="text" name="apellidos" required>
                </div>
                <div class="input-field">
                    <i class="material-icons prefix">mode_edit</i>
                    <label for="numControl">Número de control</label>
                    <input type="text" name="numControl" required>
                </div>
                <div class="input-field ">
                    <i class="material-icons prefix">group</i>
                    <select name="tipo_usuario">
                        <option value="" disabled selected>Tipo de usuario</option>
                        <option value="1">Alumno</option>
                        <option value="2">Docente</option>
                        <option value="3">Investigador</option>
                    </select>
                </div>
                <div class="input-field ">
                    <i class="material-icons prefix">school</i>
                    <select name="carrera">
                        <option value="" disabled selected>Carrera</option>
                        <option value="1">Lic. en Administración</option>
                        <option value="2">Ing. en TIC's</option>
                        <option value="3">Ing. Mecatrónica</option>
                    </select>
                </div>
                <br/><br/>
                <h5 align="center">ADEUDOS</h5>
                <table>
                <thead style="background: #3AAF88">
                        <tr>
                            <th>Libro</th>
                            <th>Préstamo</th>
                            <th>Entrega</th>
                        </tr>
                </thead>
                        <tr>
                            <td>Termodinamica 2</td>
                            <td>11/11/2019</td>
                            <td>14/11/2019</td>
                        </tr>
                </table>
            </div>
        </div>
        <!-- Comienzo de la columna que muestra información del libro/revista-->
        <div class="col s6">
        <h6>Buscar libro/revista</h6>
            <div class="row">
            <form>
                    <div class="col s7">
                    <nav>
                    <div class="nav-wrapper blue">
                        <div class="input-field">
                        <input id="search" type="search" required>
                        <label class="label-icon" for="search"><i class="material-icons">search</i>Inserte código</label>
                        <i class="material-icons">close</i>
                        </div>
                    </div>
                </nav> 
                    </div>
                    <div class="col s4">
                    <a href="" class="btn btn-sm btn" style="background: #3AAF88">Buscar</a>
                    </div>
            </form>
            </div>
            <br/><br/>
            <div class="row">
                <div class="input-field">
                    <i class="material-icons prefix">perm_identity</i>
                    <label for="nombre">Autor</label>
                    <input type="text" name="nombre" required>
                </div>
                <div class="input-field">
                    <i class="material-icons prefix">perm_identity</i>
                    <label for="nombre">Titulo</label>
                    <input type="text" name="nombre" required>
                </div>
            </div>        
        </div>
    </div>
    
    <form action="POST" action="">
    
    </form>

</div>

@endsection