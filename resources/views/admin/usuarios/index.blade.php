@extends('layouts.app')

@section('title')
Usuarios
@endsection
    
@section('content')

<h3>Usuarios</h3>
    
<div class="container">
    <div class="row" >
        <a href="{{ URL::to('/') }}/register" class="waves-effect waves-light btn right"><i class="material-icons right">add</i>Agregar Nuevo</a>
        
        <table>
        <thead>
          <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Correo Electronico</th>
              <th>Rol</th>
              <th>Acción</th>
          </tr>
        </thead>

        <tbody>
             @foreach($users as $user)
                  <tr>
                    <td>{{ $user->id  }}</td>
                    <td>{{ $user->name  }}</td>
                    <td>{{ $user->email  }}</td>
                      @foreach($user->roles as $rol)
                    <td>{{ $rol->name }}</td>
                       @endforeach
                    <td>
                        <a href="{{ URL::to('/') }}/usuario/{{ $user->id }}/edit" class="yellow darken-4 waves-effect waves-light btn"><i class="material-icons">create</i></a>
                        <a href="{{ URL::to('/') }}/usuario/{{ $user->id }}" class="blue darken-4 waves-effect waves-light btn"><i class="material-icons">remove_red_eye</i></a>
                        
                        <form method="post" action="{{ URL::to('/') }}/usuario/{{ $user->id }}">
                            {{ csrf_field() }}
     
                        <input type="hidden" name="_method" value="DELETE"/>
                            <button type="submit" class="red darken-4 waves-effect waves-light btn" ><i class="material-icons white-text">delete</i></button>
                        </form>
                      
                      </td>
                  </tr>
                    @endforeach
               
                    
        </tbody>
      </table>
        <div class="col s12 of" >
        
            
        <ul class="pagination">
        {!! $users->render() !!}
        </ul>
       
      
        </div>
    </div>
</div>
@endsection