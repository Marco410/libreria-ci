@extends('layouts.app')

@section('title')
Perfil
@endsection

@section('content')
<div class="col s12 m8 l9" >
    <div class="container" >
    <div class="row" >
        
        <h4 class="center" >Mi Perfil</h4>
         <div class="card-image center " style="background-image: url('img/src/back2.jpg'); background-repeat: no-repeat; height: 100%; " >
                     
                    <img class=" circle responsive-img margen  hoverable" src="{{ URL::to('/') }}/img/icons/hombre.svg" width="60px" height="60px" /><br/>
                    
             
                    <span class="text-bold white-text" > {{ Auth::user()->name }} |  {{ Auth::user()->email }} | No.{{ Auth::user()->id }}</span><br>
                    <span class="text-center white-text" >{{ Auth::user()->email }}</span>

             
            </div>
        </div>
         @include('errors.error')  
        <form  method="post" action="{{ URL::to('/') }}/usuario/{{ Auth::user()->id }}" >
         {{ csrf_field() }}
     
     <input type="hidden" name="_method" value="PUT"/>   
        <div class="" >     
        <div class="input-field col s6">
          <input name="name" placeholder="Nombre" id="first_name" type="text" class="validate" value="{{ Auth::user()->name }}">
          <label for="first_name">Nombre</label>
        </div>
        <div class="input-field col s6">
            @guest
                    {{ redirect('/') }}  
             <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                    @if (Route::has('register') && session('status'))
                          {{ redirect('/') }}
             <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                    @endif
            @else
             
           <select name="role_id">
                @if(Auth::user()->hasRole('admin'))
                <option selected value="1">Administrador</option>
              <option value="2">Manejador</option>
              <option value="3">Prestador</option>
              <option value="4">Procesador</option>
              <option value="5">Maestro</option>
              <option value="6">Investigador</option>
              <option value="7">Estudiante</option>
              <option value="8">Registrador</option>
                @elseif(Auth::user()->hasRole('manejador'))
            <option disabled value="1">Administrador</option>
              <option selected value="2">Manejador</option>
              <option value="3">Prestador</option>
              <option value="4">Procesador</option>
              <option value="5">Maestro</option>
              <option value="6">Investigador</option>
              <option value="7">Estudiante</option>
              <option value="8">Registrador</option>
               @elseif(Auth::user()->hasRole('prestador'))
               <option disabled value="1">Administrador</option>
              <option disabled value="2">Manejador</option>
              <option selected value="3">Prestador</option>
              <option value="4">Procesador</option>
              <option value="5">Maestro</option>
              <option value="6">Investigador</option>
              <option value="7">Estudiante</option>
              <option value="8">Registrador</option>
                @else
                 <option selected value="2">Usuario</option><option  disabled value="1">Administrador</option><option disabled value="3">Autor</option>
                @endif
               </select>
            @endguest
          <label for="last_name">Rol</label>
        </div>
        <div class="input-field col s6">
          <input name="email" placeholder="Ingresa correo" id="last_name" type="email" class="validate" value="{{ Auth::user()->email }}">
          <label for="last_name">Correo Electronico</label>
        </div>
            
            <div class="input-field col s6">
          <input name="password"  placeholder="Contraseña" id="last_name" type="password" class="validate" value="{{ Auth::user()->password }}">
          <label for="last_name">Contraseña</label>
        </div>
        
      </div>
            <input type="submit" class="btn" value="Guardar" />
        </form>
        
        <div class="row" >
        <div class="center" >
        
        <a onclick="javascript:window.history.back();" class="waves-effect waves-light btn red lighten-2"><i class="material-icons left">chevron_left</i>Regresar</a>
        
    
    </div>
</div>
        </div>
    </div>
</div>
@endsection