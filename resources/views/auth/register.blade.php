@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-panel">
                
                <div class="card-header center"><h4>{{ __('Registrar Nuevo Usuario') }}</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                      <div class="row" >     
                        <div class="input_field col s6">
                             <label for="name">{{ __('Nombre') }}</label>
                                <input placeholder="Ingresa tu Nombre" id="name" type="text" class="@error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                               
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>  
                        <div class="input_field col s6">
                            <label for="apellido_p">{{ __('Apellido Paterno') }}</label>
                                <input id="apellido_p" type="text" class="form-control @error('apellido_p') is-invalid @enderror" name="apellido_p" value="{{ old('appellido_p') }}" required autocomplete="apellido_p" autofocus placeholder="Ingresa primer apellido">
                                    
                                @error('apellido_p')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>  
                        <div class="input_field col s6">
                           

                           <label for="apellido_m" >{{ __('Apellido Materno') }}</label>
                                <input id="apellido_m" type="text" class="form-control @error('apellido_m') is-invalid @enderror" name="apellido_m" value="{{ old('appellido_m') }}" required autocomplete="apellido_m" autofocus placeholder="Ingresa segundo apellido">
                                
                                @error('apellido_m')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>

                       <div class="input_field col s6">
                           
                                <label for="email" >{{ __('Correo Electronico') }}</label>
                            
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ingresa tu correo electrónico">
                       
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div> 
                           <div class="input_field col s6">
                            <label for="password" >{{ __('Contraseña') }}</label>

                           
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Ingresa contraseña">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                       <div class="input_field col s6">
                            <label for="password-confirm" >{{ __('Confirmar Contraseña') }}</label>

                            
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirma contraseña">
                            
                        </div>
                          
                          
                          <div class="input_field col s6">
                             <div class="input-field col s12">
                                <select name="role" id="role" onchange="getSelect()">
                                  <option value="" disabled selected>Selecciona Rol</option>
                                  <option value="1">Administrador</option>
                                  <option value="2">Manejador</option>
                                  <option value="3">Prestador</option>
                                  <option value="4">Procesador</option>
                                  <option value="5">Maestro</option>
                                  <option value="6">Investigador</option>
                                  <option data-value = "7" value="7">Estudiante</option>
                                  <option data-value = "8" value="8">Registrador</option>
                                </select>
                                <label>Rol del Usuario</label>
                              </div>
                           
                          </div>
                          
                          
                          <div id="data-user" class="input-field col s6" >
                           <!--- Se agregar el input desde el javascript register.js --->
                          </div>

                       
                          
                          <h5 class="center col s12" >Contacto</h5>  
                        <div class="input_field col s4">
                           
                                <label for="colonia" >{{ __('Colonia') }}</label>
                            
                                <input id="colonia" type="text" class="form-control @error('colonia') is-invalid @enderror" name="colonia" value="{{ old('colonia') }}" required autocomplete="colonia"  placeholder="Ingresa colonia">
                       
                                @error('colonia')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>   
                          
                         <div class="input_field col s4">
                           
                                <label for="calle" >{{ __('Calle') }}</label>
                            
                                <input id="calle" type="text" class="form-control @error('calle') is-invalid @enderror" name="calle" value="{{ old('calle') }}" required autocomplete="calle" placeholder="Ingresa Calle">
                       
                                @error('calle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>
                          <div class="input_field col s4">
                           
                                <label for="telefono" >{{ __('Telefono') }}</label>
                            
                                <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" placeholder="Ingresa telefono">
                       
                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div> 
                          
                          </div> 

                        <div class="center">
                            <div class="">
                                <button type="submit" class="btn btn-primary blue-grey darken-2">
                                    {{ __('Guardar Usuario') }}
                                </button>
                                <a onclick="javascript:window.history.back();" class="waves-effect waves-light btn red lighten-2"><i class="material-icons left"></i>Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
