@extends('layouts.appCliente')

@section('title')
Carrito
@endsection
    
@section('content')
<div class="container">
    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Libro</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($apartados as $apartado)
                    <tr>
                        <th><img width="100px" src="{{ URL::to('/') }}/img/icons/book.svg"></th>
                        <th>
                            @foreach ($libros as $libro)
                                @if ($libro->id == $apartado->id_libro)
                                    {{$libro->titulo}}
                                @endif
                            @endforeach
                        </th>
                        <th><a href="eliminar_apartado/{{$apartado->id}}">Eliminar</a></th>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br/><br/><br/>
        <a href={{ URL::asset('/') }} class="btn blue darken-3">Regresar</a>
    </div>
</div>
@endsection