@extends('layouts.appCliente')

@section('title')
Inicio
@endsection
    
@section('content')
 <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center color3 accent-1">Centro de Información</h1>
        <div class="row center">
          <h5 class="header col s12 white-text">Adquirir el hábito de la lectura y rodearnos de buenos libros es construirnos un refugio moral que nos protege de casi todas las miserias de la vida”.
              </h5><h7 class="bold white-text center" >W. Somerset Maugham</h7>
        </div>
        <div class="row center">
          <a href="#" id="download-button" class="btn-large waves-effect waves-light color3b" >Ver Catálogo</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="{{ URL::to('/') }}/img/src/bookground1.jpg" alt="Unsplashed background img 1"></div>
  </div>
        
        <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center color2"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Más rápido</h5>

            <p class="light">Desde la comodidad de tu casa ya puedes apartar un libro del "Centro de Información" es facíl y rapido, solo tienes que estar previamente registrado.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center color2"><i class="material-icons">book</i></h2>
            <h5 class="center">Aparta tu Libro</h5>

            <p class="light">Ahora con nuestro nuevo sistema puedes apartar un ejemplar para que no te lo ganen, solo recuerda que tienes dos horas para recogerlo.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center color2"><i class="material-icons">home</i></h2>
            <h5 class="center">Comodidad</h5>

            <p class="light">Ya no tienes que dar vueltas y vueltas para buscar el libro que tu quieres, en nuestro catálogo puedes buscarlo más facíl desde la comodidad de tu casa.</p>
          </div>
        </div>
      </div>

    </div>
  </div>
        
    <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center color3 accent-1">Catálogo</h1>
       <div class="row center">
          <h5 class="header col s12 white-text">Puedes reservar cualquier libro que veas aquí.
              </h5>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="{{ URL::to('/') }}/img/src/bookground2.jpg" alt="Unsplashed background img 1"></div>
  </div>

        <div class="container">
          <div class="row">
            
            <div class="col l9" >
              @foreach ($libros as $libro)
                @php
                    $libro_apartado = false;
                @endphp
                @foreach ($apartados as $apartado)
                  @php
                    if ($apartado->id_libro == $libro->id) $libro_apartado = true;
                  @endphp
                @endforeach
                  @if ($libro_apartado == false)
                  <div class="col s4 m4 l4">
                    <div class="card">
                      <div class="card-image">
                        <img height="300px" width="10px" src="{{ URL::to('/') }}/img/icons/book.svg">
                        <span class="card-title"></span>
                        <a class="btn-floating halfway-fab waves-effect waves-light red" href="{{ URL::asset('apartar/'.$libro->id) }}"><i class="material-icons">add</i></a>
                      </div>
                      <div class="card-content">
                        <label>Autor: {{$libro->autor}}</label><br>
                        <label>Genero: {{$libro->contenido}}</label><br>
                        <label>Año: {{$libro->fecha_publicacion}}</label><br>
                        <label>Editorial: {{$libro->editorial}}</label><br>
                        <label>Estado: @if($libro_apartado == false) Disponible @else No disponible @endif
                        </label>
                      </div>
                    </div>
                  </div> 
                  @endif
              @endforeach

              <!--<div class="col s4 m4 l4">
              <div class="card">
                <div class="card-image">
                  <img height="300px" width="10px" src="{{ URL::to('/') }}/img/books/hadas.jpg">
                  <span class="card-title"></span>
                  <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="card-content">
                  <label>Autor:</label><br>
                  <label>Genero:</label><br>
                  <label>Año:</label><br>
                  <label>Editorial:</label>
                </div>
              </div>
            </div> <div class="col s4 m4 l4">
              <div class="card">
                <div class="card-image">
                  <img height="300px" width="10px" src="{{ URL::to('/') }}/img/books/macabros.jpg">
                  <span class="card-title"></span>
                  <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="card-content">
                  <label>Autor:</label><br>
                  <label>Genero:</label><br>
                  <label>Año:</label><br>
                  <label>Editorial:</label>
                </div>
              </div>
            </div> <div class="col s4 m4 l4">
              <div class="card">
                <div class="card-image">
                  <img height="300px" width="10px" src="{{ URL::to('/') }}/img/books/harry.png">
                  <span class="card-title"></span>
                  <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="card-content">
                  <label>Autor:</label><br>
                  <label>Genero:</label><br>
                  <label>Año:</label><br>
                  <label>Editorial:</label>
                </div>
              </div>
            </div>
                <div class="col s4 m4 l4">
              <div class="card">
                <div class="card-image">
                  <img height="300px" width="10px" src="{{ URL::to('/') }}/img/books/hombres.jpg">
                  <span class="card-title"></span>
                  <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="card-content">
                  <label>Autor:</label><br>
                  <label>Genero:</label><br>
                  <label>Año:</label><br>
                  <label>Editorial:</label>
                </div>
              </div>
            </div>
                <div class="col s4 m4 l4">
              <div class="card">
                <div class="card-image">
                  <img height="300px" width="10px" src="{{ URL::to('/') }}/img/books/hobbit.jpg">
                  <span class="card-title"></span>
                  <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="card-content">
                  <label>Autor:</label><br>
                  <label>Genero:</label><br>
                  <label>Año:</label><br>
                  <label>Editorial:</label>
                </div>
              </div>
            </div>-->
              
              </div>  
              
              <div class="col l3" >
                    <h4 class="center color5">Autores Populares</h4>
                  <ul class="collection">
                        <li class="collection-item avatar">
                          <img src="{{URL::to('/') }}/img/authors/man.svg" alt="" class="circle">
                          <span class="title">MARK MANSON</span>
                          <p>3 libros 
                          </p>
                        </li>
                        <li class="collection-item avatar">
                          <img src="{{URL::to('/') }}/img/authors/man.svg" alt="" class="circle">
                          <span class="title">VIKTOR E. FRANKL</span>
                          <p>2 libros
                          </p>
                        </li>
                        <li class="collection-item avatar">
                          <img src="{{URL::to('/') }}/img/authors/girl.svg" alt="" class="circle">
                          <span class="title">ELENA GARRO</span>
                          <p>1 libro
                          </p>
                        </li>
                        <li class="collection-item avatar">
                          <img src="{{URL::to('/') }}/img/authors/man.svg" alt="" class="circle">
                          <span class="title">GABRIEL GARCIA MARQUEZ</span>
                          <p>5 libros
                          </p>
                        </li>
                      </ul>
                  
                    <h4 class="center color5" >Noticias</h4>
                  
                      <div class="card  blue-grey lighten-5">
                        <div class="card-content black-text">
                          <span class="card-title black-text">Nuevo Libro</span>
                          <p class="blue-grey-text darken-4-text" >Ya tenemos nuevo ejemplar en la biblioteca.</p>
                        </div>
                        <div class="card-action">
                          <a href="#" style="color:#05898F" >Ver</a>
                        </div>
                      </div>
                  
                  <div class="card  blue-grey lighten-5">
                        <div class="card-content ">
                          <span class="card-title black-text">Más Espacio.</span>
                          <p class="blue-grey-text darken-4-text" >Ya contamos con más cubiculos dentro de nuestras intalaciones.</p>
                        </div>
                        <div class="card-action">
                          <a href="#" style="color:#05898F" >Ver</a>
                        </div>
                      </div>
                </div>
              
          </div>
             
            
        </div>


@endsection