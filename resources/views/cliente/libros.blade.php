@extends('layouts.appCliente')

@section('title')
Libros
@endsection
    
@section('content')

<div class="container" >
    <h4>Nuestro Catalogo</h4>
<div class="row" >
   <div class="col s12" >
              @foreach ($libros as $libro)
                @php
                    $libro_apartado = false;
                @endphp
                @foreach ($apartados as $apartado)
                  @php
                      if ($apartado->id_libro == $libro->id) $libro_apartado = true;
                  @endphp
                @endforeach
                  <div class="col s3">
                    <div class="card">
                      <div class="card-image">
                        <img height="300px" width="10px" src="{{ URL::to('/') }}/img/icons/book.svg">
                        <span class="card-title"></span>
                        <a class="btn-floating halfway-fab waves-effect waves-light red" href="{{ URL::asset('apartar/'.$libro->id) }}"><i class="material-icons">add</i></a>
                      </div>
                      <div class="card-content">
                        <label>Autor: {{$libro->autor}}</label><br>
                        <label>Genero: {{$libro->contenido}}</label><br>
                        <label>Año: {{$libro->fecha_publicacion}}</label><br>
                        <label>Editorial: {{$libro->editorial}}</label><br>
                        <label>Estado: @if($libro_apartado == false) Disponible @else No disponible @endif
                        </label>
                      </div>
                    </div>
                  </div> 
              @endforeach
</div>

</div>
    </div>
@endsection