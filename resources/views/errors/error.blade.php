@if(count($errors))

<div class="alert alert-danger" >
    <script type="text/javascript" src="{{ URL::to('/') }}/js/materialize.min.js"></script>
    
        @foreach($errors->all() as $error)
            <script>
                M.toast({html: '{{ $error }}'})   
            </script> 
        @endforeach
    
</div>


  


@endif