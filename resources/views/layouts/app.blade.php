<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Administrador</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ URL::to('/')  }}/css/otro.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{ URL::to('/')  }}/css/materialize.min.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <nav class="blue-grey darken-2 ">
            <div class="nav-wrapper" >
            <div class="container">
                
                <a class="brand-logo" href="{{ url('/') }}">
                    Administrador
                </a>
                
                
                @if (Route::has('login'))
                    @auth
                      <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="{{ URL::to('/') }}/libro">Material</a></li>
                    <li><a href="{{ URL::to('/') }}/prestamo">Prestamos</a></li>
                    <!-- <li><a href="{{ URL::to('/') }}/pedido">Pedidos</a></li> -->
                    <li><a href="{{ URL::to('/') }}/usuario">Usuarios</a></li>
                           <li>
                            <a class='dropdown-trigger white-text darken-4-text' href='#' data-target='dropdown1'>
                                    {{ Auth::user()->name }} 
                                </a>

                                <div id='dropdown1' class='dropdown-content'>
                                    <a class="blue-grey darken-4" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                                <div id="dropdown1" class="dropdown-content" ><a>Panel</a></div>
                          </li>
                         
                  </ul>
                    @endauth
                @else
               
                @endif
                    <!-- Right Side Of Navbar -->
                   
            </div>
            </div>
        </nav>

        <div class="row" >
            
 @guest
<!-- Si no esta registrado le muestra la pantalla de login -->
@if (Route::has('register'))
    <div class="col l12"  >
        @yield('content')
    </div>
@endif

 <!-- Si  esta registrado le muestra el dashboard -->
@else
                       
<!--
*
*
*

Panel de la izquierda

*
*
*
-->
     <div class="col l3" >
         
          <div class="card-image grey lighten-5 z-depth-1" >
              <ul class="collapsible">
                  
                <li >
                     <div class="card-image center" style="background-image: url({{ URL::to('/') }}/img/src/back3.jpg); width: 100%; height: 150px; background-size:cover;" >
                     
                    <a href="{{ URL::to('/') }}/perfil" ><img  class=" circle responsive-img margen  hoverable" src="{{ URL::to('/') }}/img/icons/hombre.svg" width="60px" height="60px" /></a><br/>
                         
                    <span class="text-bold white-text" > {{ Auth::user()->name }} |  {{ Auth::user()->email }}</span><br>
                         <span class="text-center white-text" >Rol</span>
                        </div>
                    </li>
            
                <li>
                   
                  <a href="{{ URL::to('/panel')}}" class="black-text" ><div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">poll</i>
                      Panel Administrador</div></a>
                  </li>
                  
                  
                
                <li class="" >
                    <div class="collapsible-header waves-effect">
                    <i class="material-icons blue-grey-text darken-2-text">collections_bookmark </i>
                  Material
                      </div>
                    <div class="collapsible-body">
                        <ul>
                            <div class="collection" >
                            <!-- Aqui estoy añadiendo ID´s de los libros, solo para que se las pueda mostrar  -->
                            <li><a class="collection-item" href="{{ URL::to('/libro')}}/create" >Añadir Material</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/libro') }}/show" >Mostrar</a></li>
                            
                            </div>
                        </ul>
                    </div>
                  </li>
                  
                  <li>
                    <div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">book</i>
                      Prestamos</div>
                       <div class="collapsible-body">
                        <ul>
                            <div class="collection" >
                            <!-- Aqui estoy añadiendo ID´s de los prestamos, solo para que se las pueda mostrar  -->
                            <li><a class="collection-item" href="{{ URL::to('/prestamo') }}" >Prestamos</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/prestamo') }}/show">Verifica usuario</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/prestamo') }}/create" >Nuevo Prestamo</a></li>
                            </div>
                        </ul>
                    </div>
                      
                  </li>
                <!-- <li>
                    <a  class="black-text modal-trigger" >
                        <div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">bookmark</i>
                      Pedidos</div></a>
                     <div class="collapsible-body">
                        <ul>
                            <div class="collection" >
                             Aqui estoy añadiendo ID´s de los pedidos, solo para que se las pueda mostrar  
                            <li><a class="collection-item" href="{{ URL::to('/pedido') }}/create" >Nuevo Pedido</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/pedido') }}/1" >Mostrar</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/pedido') }}/1/edit" >Editar</a></li>
                            </div>
                        </ul>
                    </div>
                  </li> -->
                 <!--  <li>
                    <a class="black-text" ><div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">account_circle</i>
                      Usuarios</div></a>
                       <div class="collapsible-body">
                        <ul>
                            <div class="collection" >
                            Aqui estoy añadiendo ID´s de los usuario, solo para que se las pueda mostrar 
                            <li><a class="collection-item" href="{{ URL::to('/usuario') }}/create" >Nuevo Usuario</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/usuario') }}/1" >Mostrar</a></li>
                            <li><a class="collection-item" href="{{ URL::to('/usuario') }}/1/edit" >Editar</a></li>
                            </div>
                        </ul>
                    </div>
                  </li> -->
                  <!--<li>
                    <a href="{{ URL::to('/inventario')}}" class="black-text" ><div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">storage</i>
                      Inventario</div></a>
                      
                  </li>-->
                  <li>
                    <a href="{{ URL::to('/estadisticas')}}" class="black-text" ><div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">pie_chart</i>
                      Estadisticas</div></a>
                  </li>
                  <li>
                    <a href="{{ URL::to('/') }}" class="black-text" ><div class="collapsible-header waves-effect">
                      <i class="material-icons blue-grey-text darken-2">settings</i>
                      Configuración</div></a>
                  </li>
                
                    
                
              </ul>
              <div class="card-panel grey lighten-5 z-depth-1 center" >
                        <h9>Centro de Información @2019</h9>
              
              </div>
            </div>

        </div>
            
<!--
*
*
*

Panel de la derecha

*
*
*
-->
            
    <div class="col l9" >
        
      @yield('content')
        

    </div>


    @endguest
           
        </div>
    </div>
    
    
    
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
   <script type="text/javascript" src="{{ URL::to('/') }}/js/materialize.min.js"></script>
   <script type="text/javascript" src="{{ URL::to('/') }}/js/Register/register.js"></script>
   <script type="text/javascript" src="{{ URL::to('/') }}/js/prestamo.js"></script>
    
    <script>
        
     $(document).ready(function(){
         $('.collapsible').collapsible();
          $('.modal').modal();
         $('.dropdown-trigger').dropdown();
         $('select').formSelect();
        
  });
 /*  $(document).ready(function(){
    $('.datepicker').datepicker();
  }); */
        
    </script>   
</body>
</html>
