<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CI - @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ URL::to('/')  }}/css/otro.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{ URL::to('/')  }}/css/materialize.min.css"  media="screen,projection"/>
   
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    
    <body class="has-fixed-sidenav">
        <header>
         <!-- Nav del layout cliente -->
    <div class="navbar-fixed">
        <nav class="navbar  color1b">
            <div class="mimargin">
                
             <div class="nav-wrapper">
                 <a href="#" class="brand-logo left">CI - ITM</a>
            @if (Route::has('login'))
                <ul class="right hide-on-med-and-down">
                    @auth
                       <li> <a href="{{ url('/') }}">Inicio</a></li>
                       <li> <a href="{{ url('/catalogo') }}">Libros</a></li>
                       <li> <a href="{{ url('/autores') }}">Autores</a></li>
                       <li> <a href="{{ url('/info') }}">Información</a></li>
                       <li class="" ><a href="{{ url('/carrito') }}"><i class="material-icons">shopping_cart</i></a></li>
                        <li>
                            <a class='dropdown-trigger white-text darken-4-text' href='#' data-target='dropdown1'>
                                    {{ Auth::user()->name }} 
                                </a>

                                <div id='dropdown1' class='dropdown-content'>
                                    <a class="blue-grey darken-4" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div></li>
                    
                    @else
                        <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>

                       
                    @endauth
                </ul>
            @endif

            </div>
                
          </div>
        </nav>
    </div>
            
     
           
            </header>
    <main>
        
    @yield('content')

    </main>
        
           <footer class="page-footer blue-grey darken-4">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Centro de Información</h5>
                <p class="grey-text text-lighten-4">Para registrarte tiene que ser de manera precensial en el centro de información.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Contenido</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Libros</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Agregar </a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Ver mi Carrito</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2019 Copyright Centro de Información
            <a class="grey-text text-lighten-4 right" href="#!">Más Información</a>
            </div>
          </div>
        </footer>
        
        
       <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
   <script type="text/javascript" src="{{ URL::to('/') }}/js/materialize.min.js"></script>
    
    <script>
        
     $(document).ready(function(){
         $('.collapsible').collapsible();
          $('.modal').modal();
         $('.dropdown-trigger').dropdown();
         $('select').formSelect();
         $('.parallax').parallax();
  });
        
    </script>   
    </body>
</html>
