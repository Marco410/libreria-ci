<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('layouts.appCliente');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//rutas para cliente o visitante
Route::get('/', 'GuestController@index')->name('inicio');
Route::get('/catalogo', 'GuestController@catalogo')->name('catalogo');
Route::get('/autores', 'GuestController@autores')->name('autores');
Route::get('/info', 'GuestController@info')->name('info');
Route::get('/carrito', 'GuestController@carrito')->name('carrito');

//rutas para administrador
Route::resource('/panel', 'PanelController');

//rutas para los libros
Route::resource('/libro', 'LibrosController');

 Route::post('/registrar', 'LibrosController@registrar');
 Route::post('/buscar', 'LibrosController@buscar');
 Route::post('/eliminar', 'LibrosController@destroy');
 Route::post('/editar', 'LibrosController@editar');
 Route::get('apartar/{id_libro}', 'LibrosController@apartar');
 Route::post('/ver','LibrosController@ver');
 Route::get('eliminar_apartado/{id_apartado}', 'LibrosController@eliminar_apartado');



//rutas para los prestamos
Route::resource('/prestamo', 'PrestamoController');
Route::get('/prestamo/Resumen', 'PrestamoController@index')->name('resumen');
Route::get('/prestamo/Crear', 'PrestamoController@create')->name('crear');
Route::get('/prestamo/verifica', 'PrestamoController@verifica')->name('verifica');
Route::get('/prestamo/Editar', 'PrestamoController@edit')->name('editar');
Route::get('/prestamo/Entregar', 'PrestamoController@entregar')->name('entregar');
Route::get('/prestamo/Ver-Prestamos', 'PrestamoController@show')->name('mostrar');
Route::post('/buscarPrestamo', 'PrestamoController@buscarPrestamo');
Route::post('/editarPrestamo', 'PrestamoController@editarPrestamo');
Route::post('/verificarUsuario', 'PrestamoController@buscarUsuario');
Route::get('/entregado', 'PrestamoController@entregar');
Route::get('/prestamo/{id}/entregar', 'PrestamoController@entregar');


//rutas para los pedido
Route::resource('/pedido', 'PedidoController');
Route::post('/registrarr', 'PedidoController@registrar');

//rutas para los usuarios
Route::resource('/usuario', 'UsersController');
Route::get('/perfil','UsersController@perfil');

//rutas para los inventario
Route::resource('/inventario', 'InventarioController');

//rutas para los estadisticas
Route::resource('/estadisticas', 'EstadisticsController');